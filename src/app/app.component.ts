import { AppModule } from './app.module';
import { Component } from '@angular/core';
import { IAppComponent } from './my-simple/app.interface';
import { MyDecorator } from './my-simple/my.decorator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  header: string = "Ratchanon";
  isHidden : boolean = true;
  constructor() {

  }

  submit()
  {
    console.log("TEST onSubmit");

  }

  onClick(elem:Event) {
    console.log(elem);
    console.log("TEST onclick");
    this.isHidden = !this.isHidden;

  }
  

}





