import { MyDecorator } from "./my.decorator";

@MyDecorator('Hello World')
export class MyClass {
    arrays: number[] = [100, 200, 300];
    object = { fisrtname: 'Ratchanon', lastname: 'Amphanthongpaphakul' };
  
    onFunction(param: string): string {
      return `onFunction ${param}`
    }
  
    get onGetFunction(): string {
      const sum = 2 + 2 + 2 + 2;
      return `onGetFunction :${sum}`;
    }
  
    constructor() {
      this.arrays.push(400);
      this.arrays.push(900);
    }
  
  }